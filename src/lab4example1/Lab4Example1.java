
package lab4example1;


import jade.core.Runtime;
import jade.core.Profile;
import jade.core.ProfileImpl;
import jade.wrapper.AgentContainer;
import jade.wrapper.AgentController;
import jade.wrapper.ControllerException;
import jade.wrapper.StaleProxyException;

public class Lab4Example1 
{
    public static void main(String[] args) 
    {
        Profile p = new ProfileImpl();
        Runtime rt = Runtime.instance();
        AgentContainer mc = JadeHelper.CreateContainer("mainContainer", true, "localhost", "", "1090");
        AgentContainer c1 = JadeHelper.CreateContainer("container1", false, "localhost", "", "1091");
        try
        {
            mc.start();
            c1.start();
        }catch(ControllerException e){}
            
        AgentController ag1 = JadeHelper.CreateAgent(mc, "Agent1", "lab4example1.MobileAgent", null);
        AgentController ag2 = JadeHelper.CreateAgent(mc, "Agent2", "lab4example1.CloningAgent", null);
        try
        {
            ag1.start();
            ag2.start();
        }catch(StaleProxyException e){}
        
    }
}
