package lab4example1;

import jade.core.*;

public class CloningAgent extends Agent 
{
    @Override
    public void setup() 
    {
        System.out.println("Agent " + getAID().getName() + "started...");
        try
        {
            Thread.sleep(1000);
        }catch(InterruptedException e){}
        doClone(new ContainerID("container1", null), "Agent_Clone");
    }
    
    @Override
    public void beforeClone() 
    {
        System.out.println("Cloning agent " + getAID().getName() + " ...");
    }
    
    @Override
    public void afterClone() 
    {
        System.out.println("Clone " + getAID().getName() + " ready...");
    }
}
