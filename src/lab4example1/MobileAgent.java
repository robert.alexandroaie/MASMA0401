
package lab4example1;

import jade.core.*;

public class MobileAgent extends Agent
{
    @Override
    public void setup()
    {
        System.out.println("Agent " + getAID().getName() + " started...");
        doMove(new ContainerID("container1", null));
    }
    
    @Override
    public void beforeMove()
    {
        System.out.println("Agent " + getAID().getName() + " began moving...");
    }
    
    @Override
    public void afterMove()
    {
        System.out.println("Agent " + getAID().getName() + " finished moving...");
    }
}
